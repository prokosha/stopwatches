class Timer{
	constructor(classContTimer) {
		this.countTime = 0;
		this.isStart = false;
		this.contHour = document.querySelector(classContTimer + " .hour");
		this.contMin = document.querySelector(classContTimer + " .min");
		this.contSec = document.querySelector(classContTimer + " .sec");
		this.timeButton = document.querySelector(classContTimer + " .timer-button");
		this.resetButton =  document.querySelector(classContTimer + " .reset-timer");
		this.toCorrectTime = (timeUnit) => (timeUnit < 10) ? '0' + timeUnit : timeUnit; 
		this.setTime();
		this.timeButton.addEventListener('click', this.timerStartStop.bind(this));
		this.resetButton.addEventListener('click', this.timerReset.bind(this));
	}
	startTimer () {
		this.timeOut = setTimeout(() => {
			++this.countTime;
			let hour = Math.floor(this.countTime / 3600);
			let minute = Math.floor((this.countTime - hour * 3600) / 60);
			let second = this.countTime - hour * 3600 - minute * 60;
			this.setTime(hour, minute, second);
			this.startTimer();
		}, 1000);
	}
	setTime(h=0, min=0, sec=0) {
		this.contHour.textContent = this.toCorrectTime(h);
		this.contMin.textContent = this.toCorrectTime(min);
		this.contSec.textContent = this.toCorrectTime(sec);
	}
	stopTimer() {
		clearTimeout(this.timeOut);
		this.isStart = false;
		this.timeButton.textContent = "start";
	}
	timerStartStop() {
		if (!this.isStart){
			this.timeButton.textContent= "stop";
			this.startTimer();
			this.isStart = true;
		} else {
			this.stopTimer();
		}
	}
	timerReset() {
		this.stopTimer();
		this.countTime = 0;
		this.setTime();
	}
}
const timer = new Timer(".timer");
const timer2 = new Timer(".timer2");
const timer3 = new Timer(".timer3");

